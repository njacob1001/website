const CACHE_NAME = 'cache-jacob-v13';
const urlsToCache = [
  './',
  './main.js',
  './chunk.vendor.js',
  './chunk.about.js',
  './chunk.post.js',
  './chunk.work.js',
  './chunk.blog.js',
  './manifest.json',
  './sw.js',
  '_redirect',
  './main.css',
  './favicon.ico',
  './assets/img/og-image.jpg',
  './assets/img/og-image.webp',
  './icon_16x16.png',
  './icon_32x32.png',
  './icon_48x48.png',
  './icon_64x64.png',
  './icon_96x96.png',
  './icon_128x128.png',
  './icon_192x192.png',
  './icon_256x256.png',
  './icon_384x384.png',
  './icon_512x512.png',
  './assets/img/desktop-deopies.png',
  './assets/img/desktop-grimms.png',
  './assets/img/desktop-marathons.png',
  './assets/img/desktop-portfolio.png',
  './assets/img/desktop-wok.png',
  './assets/img/kineckt.png',
  './assets/img/movile-claro.png',
  './assets/img/movile-escape.png',
  './assets/img/movile-grimms.png',
  './assets/img/movile-marathons.png',
  './assets/img/movile-wok.png',
  './assets/img/desktop-deopies.webp',
  './assets/img/desktop-grimms.webp',
  './assets/img/desktop-marathons.webp',
  './assets/img/desktop-portfolio.webp',
  './assets/img/desktop-wok.webp',
  './assets/img/kineckt.webp',
  './assets/img/movile-claro.webp',
  './assets/img/movile-escape.webp',
  './assets/img/movile-grimms.webp',
  './assets/img/movile-marathons.webp',
  './assets/img/movile-wok.webp'
];

self.addEventListener('install', e => {
  //console.log('Evento: WS instalado');
  e.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => {
        //console.log('Archivos en cache ');
        return cache.addAll(urlsToCache);
      })
      .catch(err => console.log('fallo registro de cache', err))
  );
});

self.addEventListener('activate', e => {
  //console.log('Evento: SW activado');
  const cacheList = [CACHE_NAME];

  e.waitUntil(
    caches.keys()
      .then(cachesNames => (
        Promise.all(
          cachesNames.map(cacheName => {
            if (cacheList.indexOf(cacheName) === -1) {
              return caches.delete(cacheName);
            }
          })
        )
      ))
      .then(() => {
        //console.log('El cache esta limpio y actualizado');
        return self.clients.claim();
      })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    fetch(event.request)
    .catch(function() {
      //console.log('fetch falló recuperando desde cache: ', event.request.url);
      return caches.match(event.request);
    })
  );
});

