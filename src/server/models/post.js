import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const Comment = new Schema({
  name: {
    type: String,
    require: [true, 'Name of author of comment is required']
  },
  email: {
    type: String,
    require: [true, 'YOU HAVE TO HAVE A IDUSER']
  },
  img: {
    type: String,
    default: 'https://firebasestorage.googleapis.com/v0/b/jacobimages-11a41.appspot.com/o/images%2Ficon_192x192.png?alt=media&token=d8784623-9701-4aea-96d3-953a1c73de00'
  },
  date: {
    type: Date,
    default: Date.now
  },
  text: {
    type: String,
    require: [true, 'Text of comment is required']
  }
});

const postSchema = new Schema({
  title: {
    type: String,
    require: [true, 'the title is necesary']
  },
  author: {
    type: String,
    required: [true, 'the author is necesary']
  },
  path: {
    type: String,
    required: [true, 'the path route is obligatory']
  },
  img: {
    type: String,
    required: [true, 'This post most to have a image']
  },
  webp: {
    type: String,
    required: [true, 'This post most to have a webp image']
  },
  date: {
    type: Date,
    default: Date.now //default: () => Date.now() + 7*24*60*60*1000
  },
  tags: {
    type: Array,
    required: false
  },
  category: {
    type: String,
    required: [true, 'You have to assign a category']
  },
  comments: [Comment]
});

export default mongoose.model('Post', postSchema);
