import express from 'express';
import cors from 'cors';
import mongoose from 'mongoose';
import sm from 'sitemap';
import Post from './models/post';
import serverRender from './server_render';
import api from './routes';

//const redirectToHTTPS = require('express-http-to-https').redirectToHTTPS;

//const puerto = process.env.PORT || 3000;
const app = express();
app.use(cors());
//app.use(redirectToHTTPS([/localhost:(\d{4})/], [/\/insecure/], 301));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
// Add headers
// app.use((req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001');
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//   res.setHeader('Access-Control-Allow-Credentials', true);
//   next();
// });


let sitemap = sm.createSitemap({
  hostname: 'https://jacobgonzalezweb.com',
  cacheTime: 600000,
  urls: [
    {
      url: '/',
      changefreq: 'monthly'
    },
    {
      url: '/about'
    },
    {
      url: '/blogs',
      changefreq: 'daily',
      priority: 1,
      lastmodrealtime: true
    }
  ]
 });

 const updateSitemap = new Promise((resolve, reject) => {
  Post.find({}, 'path')
   .exec((err, info) => {
     if (err) {
       reject(err);
     }
     resolve(info);
   });
});

app.get('/api/updatesitemap', (req, res) => {
  updateSitemap.then(info => {
    const baseUrl = [
      {
        url: '/',
        changefreq: 'monthly'
      },
      {
        url: '/about'
      },
      {
        url: '/blogs',
        changefreq: 'daily',
        priority: 1,
        lastmodrealtime: true
      }
    ];
    let newUrls = [];
    newUrls = info.map(item => ({ url: `/post/${item.path}` }));
    newUrls = [...baseUrl, ...newUrls];
    //console.log(newUrls);
    sitemap = sm.createSitemap({
      hostname: 'https://jacobgonzalezweb.com',
      cacheTime: 600000,
      urls: newUrls
    });
    sitemap.toXML((err, xml) => {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send(xml);
  });
  }
  );
});

// setInterval(() => (
// updateSitemap.then(info => {
//   const baseUrl = [
//     {
//       url: '/',
//       changefreq: 'monthly'
//     },
//     {
//       url: '/about'
//     },
//     {
//       url: '/blogs',
//       changefreq: 'daily',
//       priority: 1,
//       lastmodrealtime: true
//     }
//   ];
//   let newUrls = [];
//   newUrls = info.map(item => ({ url: `/post/${item.path}` }));
//   newUrls = [...baseUrl, ...newUrls];
//   //console.log(newUrls);
//   sitemap = sm.createSitemap({
//     hostname: 'https://jacobgonzalezweb.com',
//     cacheTime: 600000,
//     urls: newUrls
//   });
// }
// )), 600000);

app.get('/*.js', (req, res, next) => {
  req.url = `${req.url}.gz`;
  res.set('Content-Encoding', 'gzip');
  res.set('Content-Type', 'text/javascript');
  next();
});

app.get('/*.css', (req, res, next) => {
  req.url = `${req.url}.gz`;
  res.set('Content-Encoding', 'gzip');
  res.set('Content-Type', 'text/css');
  next();
});

app.use(express.static('public'));
app.use('/api', api);

app.get('/sitemap.xml', (req, res) => {
  sitemap.toXML((err, xml) => {
      if (err) {
        return res.status(500).end();
      }
      res.header('Content-Type', 'application/xml');
      res.send(xml);
  });
});

app.use(serverRender());

mongoose.connect('mongodb://jacob:jacobgonzalez373222@ds143163.mlab.com:43163/jacob', { useNewUrlParser: true }, (err, res) => {
  if (err) throw err;

  console.log('Base de datos ONLINE');
});

app.listen();
