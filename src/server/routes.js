import express from 'express';
import Post from './models/post';

const router = express.Router();

const { OAuth2Client } = require('google-auth-library');

process.env.CLIENT_ID = process.env.CLIENT_ID || '990335736863-2qkjcsg2mas6ongq6fl5d5hev375hhrl.apps.googleusercontent.com';

const client = new OAuth2Client(process.env.CLIENT_ID);

async function verify(token) {
  const ticket = await client.verifyIdToken({
      idToken: token,
      audience: process.env.CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
      // Or, if multiple clients access the backend:
      //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const payload = ticket.getPayload();
  console.log('verificando');
  const { name, picture, email } = payload;
  return { name, img: picture, email }
}

router.put('/comment', async (req, res, next) => {
//const { text, name, iduser, img } = req.body;
  const { path } = req.query;
  //console.log('llegado');
  //console.log(req.body);
  let newComment = await verify(req.body.idtoken)
    .catch(err => {
      return res.status(403).json({
        ok: false,
        err
      });
    });
    if(!newComment.name) {
      return;
    }
  Post.findOne({ path })
    .then(post => {
      post.comments.push({ ...newComment, text: req.body.text });
      return post.save();
    })
    .then(resp => {
      res.send({ ok: true, resp });
    })
    .catch(err => res.status(400).send(err));

});

router.put('/date', (req, res, next) => {
  const {
    path,
    days
  } = req.body;
  const dayOffset = Number(days);
  const date = Date.now() + (dayOffset * 24 * 60 * 60 * 1000);

  Post.findOneAndUpdate({ path }, { date }, { upsert: true }, (err, doc) => {
    if (err) return res.status(500).send({ error: err });
    return res.send('succesfully saved');
  });
});

router.post('/post', (req, res, next) => {
  const {
    title,
    author,
    path,
    resume,
    img,
    webp,
    tags,
    category
  } = req.body;
  const post = new Post({
    title,
    author,
    path,
    resume,
    img,
    webp,
    tags,
    category
  });

  post.save((err, postDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }

    res.json({
      ok: true,
      post: postDB
    });
  });
});

router.get('/recommended', (req, res, next) => {
  const id = req.query.category || 'tips';
  Post.find({ category: id })
    .limit(5)
    .exec((err, info) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }
      res.json({
        ok: true,
        info
      });
    });
});

router.get('/info', (req, res, next) => {
  const id = req.query.id || 'default';

  Post.find({ path: id })
    .exec((err, info) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }
      res.json({
        ok: true,
        info
      });
    });
});

router.get('/blogfilter', (req, res, next) => {
  const filter = Number(req.query.filter) || 0;
  let strFilter;
  switch (filter) {
    case 0: strFilter = 'most recent';
      break;
    case 1: strFilter = 'front end';
      break;
    case 2: strFilter = 'back end';
      break;
    case 3: strFilter = 'engineering';
      break;
    case 4: strFilter = 'video games';
      break;
    case 5: strFilter = 'tips';
      break;
    default: strFilter = 'most recent';
      break;
  }
  const objFilter = filter !== 0 ? { category: strFilter } : {};
  Post.find(objFilter, `key title img webp path author ${filter === 0 ? 'category' : ''}`)
  .exec((err, blogs) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      });
    }
    Post.countDocuments({}, (err, conteo) => {
      res.json({
        ok: true,
        total: conteo,
        blogs
      });
    });
  });
});

router.get('/blogs', (req, res, next) => {
  const desde = req.query.start || 0;
  const hasta = req.query.end || 3;
  //para filtrar datos agregamos 'atributo atributo atributo'
  //asi Post.find({}, 'title img author')
  Post.find({}, 'key title img webp path author category')
    .skip(Number(desde))
    .limit(Number(hasta))
    .exec((err, blogs) => {
      if (err) {
        return res.status(400).json({
          ok: false,
          err
        });
      }
      Post.countDocuments({}, (err, conteo) => {
        res.json({
          ok: true,
          total: conteo,
          blogs
        });
      });
    });
});

export default router;
