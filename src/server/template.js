const Html = (body, data, helmet) => {
  const prop = {
    urlBase: 'https://jacobgonzalezweb.com/',
    theme: '2B303A'
  };

  return (`
    <!DOCTYPE html>
    <html lang="en" prefix="og: http://ogp.me/ns#" itemscope itemtype="http://schema.org/Blog">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="HandheldFriendly" content="true">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        ${helmet ? helmet.title.toString() : '<title>Helmet no encontrado</title>'}
        ${helmet ? helmet.meta.toString() : ''}
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">
        <base href="/">
        <noscript><p>Ncesita tener habilitado JavaScript para poder visualizar contenido</p></noscript>
        <meta name="HandheldFriendly" content="true">
        <meta name="MobileOptimized" content="320">
        <meta name="theme-color" content="#${prop.theme}">
        <meta name="msapplication-TileColor" content="${prop.theme}">
        <meta name="msapplication-TileImage" content="${prop.theme}">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <link rel="apple-touch-startup-image" href="./assets/img/og-image.jpg">
        <link rel="apple-touch-icon" href="./assets/img/og-image.jpg">
        <link rel="author" href="https://plus.google.com/112455610659481296930">
        <link rel="publisher" href="https://plus.google.com/112455610659481296930">
        <link rel="manifest" href="./manifest.json">
        <link rel="shortcut icon" href="./favicon.ico">
        <link href="./main.css" rel="stylesheet">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127796428-2"></script>
      </head>
      <body>
        <div id="root">${body}</div>
        <script>window.initialState = ${data}</script>
        <script type="text/javascript" src="./chunk.vendor.js"></script>
        <script type="text/javascript" src="./main.js"></script>
        <script>
          if ("serviceWorker" in navigator) {
            window.addEventListener("load", function() {
              navigator.serviceWorker.register("/sw.js");
            });
          }
        </script>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      </body>
    </html>`);
};

export default Html;
