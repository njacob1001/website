import React, { PureComponent } from 'react';
import ScaleIndicator from './scale_indicator';
import profileImage from './../../../assets/img/og-image.jpg';
import pprofileImage from './../../../assets/img/og-image.webp';
import fallbackimg from './../../../assets/img/fallback-image.svg';
import SkillSet from '../data';
import AutoHelmet from './../../../common/utils/AutoHelmet';
//import './_about.scss';

class AboutPage extends PureComponent {
  componentDidMount() {
    if (window) {
      window.scrollTo(0, 0);
    }
  }
  render() {
    return (
      <section className="About-wrapper MainContent">
        <AutoHelmet
          canonical="https://jacobgonzalezweb.com/about"
          title="About me - Jacob González"
          description="I'm able to create web applications, videogames or websites for business, I use modern technologies for improve the performance of the software"
          type="website"
          image="/assets/img/og-image.jpg"
          id="174721253471154"
        />
        <main className="About-container">
          <picture className="About-image">
            <source srcSet={pprofileImage} type="image/webp" />
            <source srcSet={profileImage} type="image/jpg" />
            <img src={fallbackimg} alt="Jacob Gonzalez" />
          </picture>
          <article className="About-main">
            <h1 className="About-title">Hi! my name is Jacob Gonzalez, Currently I'm freelancer developer</h1>
            <p className="About-description">
              I've working as freelance developer for 4 years in
              diferents projects like videogames
              ans websites or web applications. I have
              always been insterested in the web development,
              currently, I use ReactJS and other libraries for
              improve the user experience of my web projects
            </p>
          </article>
        </main>
        <aside className="About-skillContainer">
          <div className="About-skillwrapper">
            <h2 className="About-skillTitle">Skillset</h2>
            {SkillSet.map(item => (
              <div key={item.name.slice(0, 3)} className="About-skill">
                <strong>{item.name}</strong>
                <span>{item.items}</span>
                <ScaleIndicator value={item.level} />
              </div>
            ))}
          </div>
        </aside>
      </section>
    );
  }
}

export default AboutPage;
