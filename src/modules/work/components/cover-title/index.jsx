import React from 'react';
//import './_cover_title.scss';

const Presentation = () => (
  <section className="Presentation-wrapper">
    <div className="Presentation-container">
      <h2 className="Presentation-title">Multimedia Engineer</h2>
      <p className="Presentation-subtext">The perfect integration between information, design and technology is multimedia engineering.</p>
      <h1 className="Presentation-name">Jacob González</h1>
    </div>
  </section>
);

export default Presentation;
