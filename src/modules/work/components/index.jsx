import React from 'react';
import Presentation from './cover-title';
import Projects from './projects';
import AutoHelmet from './../../../common/utils/AutoHelmet';
//import LoadingBlog from './../../../common/ui/loagin-blog';

const WorkPage = () => (
  <main className="MainContent">
    <AutoHelmet
      canonical="https://jacobgonzalezweb.com/"
      title="Jacob González Multimedia Engineer, interactive websites and movile apps"
      description="More than 2.4 billion people use the internet every day, I can make your business visible with a website, or web application, so you can be  you can be sure that your website is built with the best technologies so far."
      type="website"
      image="/assets/img/og-image.jpg"
      id="174721253471154"
    />
    <Presentation />
    <Projects />
  </main>
);

export default WorkPage;
