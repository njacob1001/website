/* Videogamesprojects */
import deoPies from './../../../../assets/img/desktop-deopies.png';
import deoPieskinect from './../../../../assets/img/kineckt.png';
import escape from './../../../../assets/img/movile-escape.png';
import claro from './../../../../assets/img/movile-claro.png';

/*webp */
import pdeoPies from './../../../../assets/img/desktop-deopies.webp';
import pdeoPieskinect from './../../../../assets/img/kineckt.webp';
import pescape from './../../../../assets/img/movile-escape.webp';
import pclaro from './../../../../assets/img/movile-claro.webp';

const gamesData = [
  {
    id: 'escape',
    name: 'escape videogame',
    images: [
      {
        src: [
          {
            type: 'image/webp',
            image: pescape
          },
          {
            type: 'image/png',
            image: escape
          }
        ],
        alt: 'escape movile videogame'
      }
    ]
  },
  {
    id: 'deopies',
    name: 'Woman Runner (Colombia)',
    images: [
      {
        src: [
          {
            type: 'image/webp',
            image: pdeoPies
          },
          {
            type: 'image/png',
            image: deoPies
          }
        ],
        alt: 'deoPies videogame'
      },
      {
        src: [
          {
            type: 'image/webp',
            image: pdeoPieskinect
          },
          {
            type: 'image/png',
            image: deoPieskinect
          }
        ],
        alt: 'kineck'
      }
    ]
  },
  {
    id: 'claro',
    name: 'Claro movile',
    images: [
      {
        src: [
          {
            type: 'image/webp',
            image: pclaro
          },
          {
            type: 'image/png',
            image: claro
          }
        ],
        alt: 'claro videogame'
      }
    ]
  }
];

export default gamesData;
