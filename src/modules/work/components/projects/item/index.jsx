import React from 'react';
import imgfallback from './../../../../../assets/img/fallback-image.svg';
//import './_project_item.scss';

const ItemProject = ({ position, lastName, imgs, name }) => (
  <figure className={`Work-container ${lastName}`}>
    <span className="Work-numerator">{position}</span>
    {imgs.map((img, index) => (
      <picture key={`pp${img.alt}`} className={index === 0 ? 'Work-desktop' : 'Work-movile'}>
        {img.src.map((src, srcin) => (<source key={`src${img.alt}${index}${srcin}`} srcSet={src.image} type={src.type} />))}
        <img src={imgfallback} alt={img.alt} />
      </picture>
    ))}
    <figcaption className="Work-name">{name}</figcaption>
  </figure>
);

export default ItemProject;
