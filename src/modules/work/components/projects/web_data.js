/* Web projects */
import grimmsMovile from './../../../../assets/img/movile-grimms.png';
import grimmsDesktop from './../../../../assets/img/desktop-grimms.png';
import wokMovile from './../../../../assets/img/movile-wok.png';
import wokdesktop from './../../../../assets/img/desktop-wok.png';
import marathonsMovile from './../../../../assets/img/movile-marathons.png';
import marathonsDesktop from './../../../../assets/img/desktop-marathons.png';
import portfolioMovile from './../../../../assets/img/movile-portfolio.png';
import portfolioDesktop from './../../../../assets/img/desktop-portfolio.png';
/** webp */
import pgrimmsMovile from './../../../../assets/img/movile-grimms.webp';
import pgrimmsDesktop from './../../../../assets/img/desktop-grimms.webp';
import pwokMovile from './../../../../assets/img/movile-wok.webp';
import pwokdesktop from './../../../../assets/img/desktop-wok.webp';
import pmarathonsMovile from './../../../../assets/img/movile-marathons.webp';
import pmarathonsDesktop from './../../../../assets/img/desktop-marathons.webp';
import pportfolioMovile from './../../../../assets/img/movile-portfolio.webp';
import pportfolioDesktop from './../../../../assets/img/desktop-portfolio.webp';

const webData = [
  {
    id: 'grimms',
    name: 'grimms kindergarten',
    images: [
      {
        src: [
          {
            type: 'image/webp',
            image: pgrimmsDesktop
          },
          {
            type: 'image/png',
            image: grimmsDesktop
          }
        ],
        alt: 'Grimms desktop'
      },
      {
        src: [
          {
            type: 'image/webp',
            image: pgrimmsMovile
          },
          {
            type: 'image/png',
            image: grimmsMovile
          }
        ],
        alt: 'Grimms movile'
      }
    ]
  },
  {
    id: 'wok',
    name: 'wok restaurant',
    images: [
      {
        src: [
          {
            type: 'image/webp',
            image: pwokdesktop
          },
          {
            type: 'image/png',
            image: wokdesktop
          }
        ],
        alt: 'Wok desktop'
      },
      {
        src: [
          {
            type: 'image/webp',
            image: pwokMovile
          },
          {
            type: 'image/png',
            image: wokMovile
          }
        ],
        alt: 'Wok movile'
      }
    ]
  },
  {
    id: 'marathons',
    name: 'marathons on the world',
    images: [
      {
        src: [
          {
            type: 'image/webp',
            image: pmarathonsDesktop
          },
          {
            type: 'image/png',
            image: marathonsDesktop
          }
        ],
        alt: 'marathons desktop'
      },
      {
        src: [
          {
            type: 'image/webp',
            image: pmarathonsMovile
          },
          {
            type: 'image/png',
            image: marathonsMovile
          }
        ],
        alt: 'marathons movile'
      }
    ]
  },
  {
    id: 'portfolio',
    name: 'my first portfolio',
    images: [
      {
        src: [
          {
            type: 'image/webp',
            image: pportfolioDesktop
          },
          {
            type: 'image/png',
            image: portfolioDesktop
          }
        ],
        alt: 'portfolio desktop'
      },
      {
        src: [
          {
            type: 'image/webp',
            image: pportfolioMovile
          },
          {
            type: 'image/png',
            image: portfolioMovile
          }
        ],
        alt: 'portfolio movile'
      }
    ]
  }
];

export default webData;
