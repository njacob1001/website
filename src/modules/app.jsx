import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { HelmetProvider } from 'react-helmet-async';
import { Router, Route, Switch } from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import ReactGA from 'react-ga';
import MinimalHeader from '../common/ui/minimal-header';
import MinimalFooter from '../common/ui/minimal-footer';
import AsyncComponent from './../common/utils/asyncComponent';
import store from './../common/configureStore';
import module from './module';

const register = module(store);

const Work = () => import(/* webpackChunkName: "work" */ './../modules/work');
const About = () => import(/* webpackChunkName: "about" */ './../modules/about');
const Blog = () => register('blog', import(/* webpackChunkName: "blog" */'./../modules/blog'));
const Post = () => register('post', import(/* webpackChunkName: "post" */ './../modules/post'));

const helmetContext = {};
const history = createHistory();

history.listen(location => {
	ReactGA.set({ page: location.pathname })
	ReactGA.pageview(location.pathname)
});

class App extends Component {
  componentDidMount() {
		ReactGA.pageview(window.location.pathname)
  }

  render() {
    return(
      <HelmetProvider context={helmetContext}>
        <Provider store={store}>
          <Router history={history}>
            <React.Fragment>
              <MinimalHeader />
                <Switch>
                  <Route path="/" exact component={() => <AsyncComponent moduleProvider={Work} />} />
                  <Route path="/about" component={() => <AsyncComponent moduleProvider={About} />} />
                  <Route path="/blogs" component={() => <AsyncComponent moduleProvider={Blog} />} />
                  <Route path="/post/:id" component={({match}) => <AsyncComponent match={match} moduleProvider={Post} />} />
                  <Route component={() => <AsyncComponent moduleProvider={Work} />} />
                </Switch>
              <MinimalFooter />
            </React.Fragment>
          </Router>
        </Provider>
      </HelmetProvider>
    );
  }
}

export default App;
