import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import ReactMarkdown from 'react-markdown';
import { fetchInfo } from './actions';
//import { isFirstRender } from './../../../common/utils/data';
import CodeBlock from './codeblock';
import LoadingBlog from './../../../common/ui/loagin-blog';
import AutoHelmet from './../../../common/utils/AutoHelmet';
import BlogCard from './../../../common/ui/blog-card';
import ShareButtons from './../../../common/ui/share-buttons';
import CommentBox from './comments_box';

class Post extends PureComponent {
  static initialAction(fetchFrom, url) {
    if (fetchFrom === 'server') {
      let onlyPath = url.split('t/')[1];
      if (onlyPath.indexOf('?') !== -1) {
        onlyPath = onlyPath.split('?')[0];
      }
      return fetchInfo(fetchFrom, { id: onlyPath });
    }
    return fetchInfo(fetchFrom, { id: url });
  }

  constructor(props) {
    super(props);
    this.state = {
      recommended: null
    };
    this.editor = React.createRef();
    this.handlePost = this.handlePost.bind(this);
  }

  componentDidMount() {
    (window.adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: 'ca-pub-9596502450064095',
      enable_page_level_ads: true
    });

    if (window) {
      window.scrollTo(0, 0);
    }
    this.props.dispatch(Post.initialAction('client', this.props.match.params.id));

    const newHeader = document.getElementById('header');
    if (newHeader) {
      newHeader.style.backgroundColor = 'var(--dark-color)';
      newHeader.style.position = 'absolute';
    }
  }

  componentDidUpdate() {
    const codes = [...document.querySelectorAll('.Post-body > pre')];
    codes.map(item => {
      if (item) {
        item.style.width = '100%';
        item.style.background = 'var(--dark-color)';
      }
      return true;
    });
    console.log('actualizado');
    //fetch(`http://localhost:3000/api/recommended?category=${this.props.info.category}`)
    //fetch(`https://jacobgz.herokuapp.com/api/recommended?category=${this.props.info.category}`)
    if (this.props.info.category) {
      fetch(`https://jacobgonzalezweb.com/api/recommended?category=${this.props.info.category}`)
      .then(respon => respon.text())
      .then(recommended => {
        this.setState({ recommended });
        if (window) {
          window.scrollTo(0, 0);
        }
      })
      .catch(err => console.log(err));
    }
  }

  componentWillUnmount() {
    const oldHeader = document.getElementById('header');
    if (oldHeader) {
      oldHeader.style.backgroundColor = 'transparent';
      oldHeader.style.position = 'fixed';
    }
    window.adsbygoogle = [];
  }

  handlePost(path) {
    this.props.dispatch({
      type: 'FETCH_INFO_SUCCESS',
      payload: { ...this.props.info, body: null }
    });
    this.props.dispatch(Post.initialAction('client', path));
  }

  render() {
    const { title, resume, img, path, comments } = this.props.info;
    const { recommended } = this.state;
    const commesasasnts = [
      {
        name: 'Karen',
        text: 'as actitudes que debe adoptar éste para inspirar confianza a su auditorio. Escribió Aristóteles en el Libro I de su Retórica',
        img: 'https://lh6.googleusercontent.com/-ipXmh_KZ4zk/AAAAAAAAAAI/AAAAAAAAH3Y/GudBfI9ZF68/s96-c/photo.jpg',
        date: '2018-11-18T22:49:34.701Z'
      },
      {
        name: 'jacob',
        text: 'No son los hechos los que cambian el comportamiento de la gente',
        img: 'https://lh6.googleusercontent.com/-ipXmh_KZ4zk/AAAAAAAAAAI/AAAAAAAAH3Y/GudBfI9ZF68/s96-c/photo.jpg',
        date: '2018-11-12T22:49:34.701Z'
      },
      {
        name: 'juan',
        text: ' Efectivamente, como argumentos emocionales pueden utilizarse las historias, anécdotas, analogías, metáforas, símiles',
        img: 'https://lh6.googleusercontent.com/-ipXmh_KZ4zk/AAAAAAAAAAI/AAAAAAAAH3Y/GudBfI9ZF68/s96-c/photo.jpg',
        date: '2018-10-18T22:49:34.701Z'
      }
    ];

    return (
      <main className="Post-container MainContent">
        <AutoHelmet
          canonical={`https://jacobgonzalezweb.com/post/${path}`}
          title={title || 'Cargando...'}
          description={resume || 'Cargando...'}
          type="article"
          image={img}
          id="174721253471154"
        />
        <article ref={this.editor} className="Post-wrapper">
          <ShareButtons url={`https://jacobgonzalezweb.com/post/${path}`} />
          {this.props.info.body ?
          <ReactMarkdown
            className="Post-body"
            escapeHtml={false}
            source={this.props.info.body}
            renderers={{ code: CodeBlock }}
          /> : <LoadingBlog className="Full Blog Post" />}
          <strong className="Post-end">Thank you for read :)</strong>
          {/* <CommentBox path={path} content={comments} /> */}
        </article>
        <aside className="Post-aside">
            <h3 className="Post-subsection">RECOMMENDED POSTS</h3>
            {recommended
              ? JSON.parse(recommended).info
              .filter(req => req.path !== path)
              .map(element => (
                <BlogCard action={this.handlePost} {...element} />
              ))
              : <LoadingBlog className="Full Blog Post" />
            }
        </aside>
      </main>
    );
  }
}

export default connect(({ post }) => ({
  info: post.info
}), null)(Post);
