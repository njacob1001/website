import React, { PureComponent } from 'react';
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import { GoSignIn, GoSignOut } from 'react-icons/go';
import timeAgo from 'node-time-ago';

class CommentBox extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      login: false,
      uilogin: '',
      user: null,
      newComment: null
    };
    this.text = React.createRef();
    this.handleLogin = this.handleLogin.bind(this);
    this.handleAuthError = this.handleAuthError.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleComment = this.handleComment.bind(this);
  }
  handleComment(e) {
    e.preventDefault();
    if (this.state.login && this.text.current.value.length < 50) {
      console.log('enviando comentario');
      const { user } = this.state;
      console.table({ ...user, text: this.text.current.value });
      //'localhost:3000/api/comment'
      //'https://jacobgz.herokuapp.com

      fetch(`https://jacobgz.herokuapp.com/api/comment?path=${this.props.path}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ ...user, text: this.text.current.value })
      }).then(() => {
        this.setState({
          newComment: (
            <div className="Comment-container">
            <div className="Comment-img">
              <img src={this.state.user.img} className="Comment-imgsrc" alt="jajajaj" />
            </div>
            <div className="Comment-content">
              <div className="Comment-info">
                <h3 className="Comment-author">
                  {this.state.user
                    .name
                    .toLowerCase()
                    .replace(/([^a-z])([a-z])(?=[a-z]{2})|^([a-z])/g, (_, g1,g2,g3) => (typeof g1 === 'undefined') ? g3.toUpperCase() : g1 + g2.toUpperCase())}
                </h3>
                <div className="Comment-date">Just now</div>
              </div>
              <p className="Comment-text">{this.text.current.value}</p>
            </div>
          </div>
          )
        })
      })
      .catch(err => console.log(err));
    } else {
      this.setState({ uilogin: 'is-active' });
    }
  }
  handleLogin(response) {
    console.log(response);
    this.setState({
      login: true,
      uilogin: '',
      user: {
        idtoken: response.tokenObj.id_token,
        name: response.profileObj.name,
        img: response.profileObj.imageUrl
      }
    });
  }
  handleAuthError(err) {
    console.log('error');
    console.log(err);
    console.log(err.details);
    this.setState({ uilogin: 'is-active' });
  }
  handleLogout(response) {
    this.setState({ login: false });
    console.log(response);
  }

  render() {
    const { login, uilogin } = this.state;
    return (
      <section className="Comments-container">
        <div className="Comments-wrapper">
        <form className="Comments-form">
          <div className="Comments-main">
            <textarea
              ref={this.text}
              maxLength="500"
              className="Comments-input"
              name="comment"
              id="comment"
              placeholder="Say something"
            />
            <div className={`Comments-login ${uilogin}`}>
            {login && window
              ? <GoogleLogout className="Comments-google" onLogoutSuccess={this.handleLogout}>
                  <GoSignOut />
                </GoogleLogout>
              : <GoogleLogin
                   className="Comments-google"
                  clientId="990335736863-2qkjcsg2mas6ongq6fl5d5hev375hhrl.apps.googleusercontent.com"
                  onSuccess={this.handleLogin}
                  onFailure={this.handleAuthError}
              ><GoSignIn /></GoogleLogin>}
            </div>
          </div>
          <button onClick={(e) => this.handleComment(e)} type="submit" className="Comments-button">Comment</button>
        </form>
        {this.state.newComment}
        {this.props.content && this.props.content.map((comment, index) => (
          <div className="Comment-container" key={index * 858}>
            <div className="Comment-img">
              <img src={comment.img} className="Comment-imgsrc" alt="jajajaj" />
            </div>
            <div className="Comment-content">
              <div className="Comment-info">
                <h3 className="Comment-author">
                  {comment
                    .name
                    .toLowerCase()
                    .replace(/([^a-z])([a-z])(?=[a-z]{2})|^([a-z])/g, (_, g1,g2,g3) => (typeof g1 === 'undefined') ? g3.toUpperCase() : g1 + g2.toUpperCase())}
                </h3>
                <div className="Comment-date">{timeAgo(comment.date.replace('T', '(').replace('Z', ')'))}</div>
              </div>
              <p className="Comment-text">{comment.text}</p>
            </div>
          </div>
        ))}
        </div>
      </section>
    );
  }
}

export default CommentBox;
