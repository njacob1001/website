// Api
import blogApi from './api';

// Action Types
import { FETCH_INFO } from './action_types';

export const fetchInfo = (fetchingFrom, query) => dispatch => {
  const requestPosts = () => ({
    type: FETCH_INFO.request()
  });
  const receivedPosts = info => ({
    type: FETCH_INFO.success(),
    payload: info
  });

  dispatch(requestPosts());

  return blogApi.getAllPosts(query, fetchingFrom)
    .then(info => dispatch(receivedPosts(info)));
};
