export const FETCH_INFO = {
  request: () => 'FETCH_INFO_REQUEST',
  success: () => 'FETCH_INFO_SUCCESS',
  error: () => 'FETCH_INFO_ERROR'
};
