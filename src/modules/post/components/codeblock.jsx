import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter/prism';
import { okaidia } from 'react-syntax-highlighter/styles/prism';

export default class CodeBlock extends React.PureComponent {
  render() {
    const { language, value } = this.props;

    return (
      <SyntaxHighlighter
        language={language}
        style={okaidia}
      >
        {value}
      </SyntaxHighlighter>
    );
  }
}
