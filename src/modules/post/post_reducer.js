const initialState = {
  info: {}
};

export default function post(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_INFO_SUCCESS': {
      return {
        ...state,
        info: action.payload
      };
    }
    default:
      return state;
  }
}
