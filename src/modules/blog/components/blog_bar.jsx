import React from 'react';
//import SearchBar from './search_bar';

const BlogBar = ({ action, active }) => (
  <div className="Bbar-wrapper">
    <div className="Bbar-container">
      <ul className="Bbar-items">
        <li onClick={() => action(0)} className={`Bbar-item ${active === 0 ? 'is-active' : ''}`}>
          <span>Most Recent</span>
        </li>
        <li onClick={() => action(1)} className={`Bbar-item ${active === 1 ? 'is-active' : ''}`}>
          <span>Front End</span>
        </li>
        <li onClick={() => action(2)} className={`Bbar-item ${active === 2 ? 'is-active' : ''}`}>
          <span>Back End</span>
        </li>
        <li onClick={() => action(3)} className={`Bbar-item ${active === 3 ? 'is-active' : ''}`}>
          <span>Engineering</span>
        </li>
        <li onClick={() => action(4)} className={`Bbar-item ${active === 4 ? 'is-active' : ''}`}>
          <span>Videogames</span>
        </li>
        <li onClick={() => action(5)} className={`Bbar-item ${active === 5 ? 'is-active' : ''}`}>
          <span>Tips</span>
        </li>
      </ul>
    </div>
  </div>
);

export default BlogBar;
