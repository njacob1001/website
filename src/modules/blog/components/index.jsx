import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import BlogBar from './blog_bar';
import BlogCard from './../../../common/ui/blog-card';
import AutoHelmet from './../../../common/utils/AutoHelmet';
import { fetchPosts } from './actions';
import { apiFetch } from './../../../common/utils/api';
import { isFirstRender } from './../../../common/utils/data';
import LoadingBlog from './../../../common/ui/loagin-blog';
//import './_blog.scss';

class BlogPage extends PureComponent {
  static initialAction(fetchFrom) {
    return fetchPosts(fetchFrom, { start: 0, end: 9 });
  }
  constructor(props) {
    super(props);
    this.state = {
      menu: 0,
      allowFilter: false
    };
    this.handleFilter = this.handleFilter.bind(this);
  }

  componentDidMount() {
    if (isFirstRender(this.props.posts)) {
      this.props.dispatch(BlogPage.initialAction('client'));
    }
    if (window) {
      window.scrollTo(0, 0);
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props.posts && this.state.allowFilter) {
      this.props.dispatch({
        type: 'FETCH_POSTS_SUCCESS',
        payload: null
      });
      const { menu } = this.state;
      apiFetch('blogfilter', 'client', { filter: menu })
      .then(posts => {
        this.setState({ allowFilter: false });
        this.props.dispatch({
          type: 'FETCH_POSTS_SUCCESS',
          payload: posts.blogs
        });
      });
    }
  }
  handleFilter(newValue) {
    this.setState({
      menu: newValue,
      allowFilter: true
    });
  }
  render() {
    const { search, posts } = this.props;
    return (
      <section className="Blog-container MainContent">
        <AutoHelmet
          canonical="https://jacobgonzalezweb.com/blogs"
          title="Blogs de programacion y desarrollo de videojuegos y mas temas relacionados al mundo digital"
          description="Tutoriales, cursos, noticias, todo sobre videojuegos, desarrolo web, programación en general,  realidad aumentada y procesamiento de señales"
          type="website"
          image="/assets/img/og-image.jpg"
          id="174721253471154"
        />
        <h1 className="Blog-header">
          <strong className="Blog-headerTitle">LET’S LEARN ABOUT THE WORLD OF PROGRAMMING</strong>
        </h1>
        <BlogBar action={this.handleFilter} active={this.state.menu} />
        {search && <h2>Buscar por: "{search}"</h2>}
        <main className="Blog-list">
          {posts
            ? posts.map(elem => <BlogCard {...elem} />)
            : <LoadingBlog className="Blogs" />}
        </main>
      </section>
    );
  }
}

export default connect(({ blog }) => ({
  posts: blog.posts,
  search: blog.search
}), null)(BlogPage);
