import React from 'react';
import Helmet from 'react-helmet-async';

const AutoHelmet = ({ canonical, title, description, type, image, id }) => (
  <Helmet>
    <link rel="canonical" href={canonical} />
    <title>{title}</title>
    <meta name="description" content={description} />
    <meta itemProp="name" content={title} />
    <meta itemProp="description" content={description} />
    <meta itemProp="image" content={image} />
    <meta property="og:image" content={image} />
    <meta property="og:title" content={title} />
    <meta property="og:type" content={type} />
    <meta property="og:url" content={canonical} />
    <meta property="og:description" content={description} />
    <meta name="twitter:title" content={title} />
    <meta name="twitter:description" content={description} />
    <meta name="twitter:image:src" content={image} />
    <meta property="fb:app_id" content={id} />
  </Helmet>
);
 export default AutoHelmet;
