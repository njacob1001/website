// Dependencies
import 'isomorphic-fetch';
import queryString from 'query-string';

export function apiEndpoint(endpoint, qs, fetchingFrom) {
  let query = '';
  let apiUrl = 'https://jacobgonzalezweb.com'; //http://localhost:3000

  if (qs) {
    query = `?${qs}`;
  }

  if (fetchingFrom === 'server') {
    apiUrl = 'https://jacobgonzalezweb.com';//https://jacobgz.herokuapp.com
  }

  return `${apiUrl}/api/${endpoint}${query}`;
}

export function apiFetch(endpoint, options = {}, query = false) {
  let qs;
  const { fetchingFrom = 'client' } = options;

  delete options.fetchFrom;

  if (query) {
    qs = queryString.stringify(query);
  }

  const fetchOptions = apiOptions(options);
  const fetchEndpoint = apiEndpoint(endpoint, qs, fetchingFrom);
  //console.log(`endpoint: ${fetchEndpoint}`);

  //const reExp = /(\/api\/info\/)/;
  let result = {};
  return endpoint === 'info'
    ? fetch(fetchEndpoint, fetchOptions)
        .then(response => response.json())
        .then(json => {
          result = json.info[0];
          return fetch(`https://raw.githubusercontent.com/njacob1001/post/master/${result.path}.md`)
            .then(res => res.text())
            .then(post => ({ ...result, body: post }));
        }).then(content => content)
    : fetch(fetchEndpoint, fetchOptions).then(response => response.json());
}

export function apiOptions(options = {}) {
  const {
    method = 'GET',
    headers = {
      'Content-Type': 'application/json'
    },
    body = false
  } = options;

  const newOptions = {
    method,
    headers,
    credentials: 'include'
  };

  if (body) {
    newOptions.body = body;
  }

  return newOptions;
}
