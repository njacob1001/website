import React from 'react';
import { Link } from 'react-router-dom';
import LoadingImage from './../../../common/ui/image-loading';

const BlogCard = ({ img, webp, title, action, path, category }) => (
  action
  ?
  <Link onClick={() => action(path)} to={`/post/${path}`} className="Blog-card">
    <LoadingImage src={img} srcwp={webp} name={title} />
    {category && <strong className="Blog-category">{category}</strong>}
    <h3 className="Blog-name">{title}</h3>
  </Link>
  :
  <Link to={`/post/${path}`} className="Blog-card">
    <LoadingImage src={img} srcwp={webp} name={title} />
    {category && <strong className="Blog-category">{category}</strong>}
    <h3 className="Blog-name">{title}</h3>
  </Link>
);

export default BlogCard;
