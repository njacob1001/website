import React from 'react';

const LoadingBlog = props => (
  <div {...props} id="loading-blog">
    <div className="Loading-blog">
      <div className="Loading-loader"><div></div></div>
      <div className="Loading-loader"><div></div></div>
      <div className="Loading-loader"><div></div></div>
    </div>
  </div>
);

export default LoadingBlog;
