import React, { PureComponent } from 'react';
import { FaSpinner } from 'react-icons/fa';

class LoadingImage extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
    this.handleLoad = this.handleLoad.bind(this);
  }
  handleLoad() {
    this.setState({ loaded: true });
  }

  render() {
    const { loaded } = this.state;
    const { src, srcwp, name } = this.props;
    const status = loaded ? 'completed' : 'loading';
      return (
        <div className={`Loadimg-container ${status}`}>
          {!this.state.loaded
           ? <div className={`Loadimg-modal ${status}`}>
                <FaSpinner className="Loadimg-icon" />
              </div>
            : <div></div>}
        <picture className={`Loadimg-img ${status}`} onLoad={this.handleLoad}>
          <source srcSet={srcwp} type="image/webp" />
          <source srcSet={src} type="image/jpg" />
          <img src={src} alt={name} />
        </picture>
      </div>
    );
  }
}

export default LoadingImage;
