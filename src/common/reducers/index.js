// Dependencies
import { combineReducers } from 'redux';

// Shared Reducers
//import device from './deviceReducer';
import blog from './../../modules/blog/blog_reducer';
import post from './../../modules/post/post_reducer';

const rootReducer = combineReducers({
  blog,
  post
});

export default rootReducer;
