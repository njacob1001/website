import WorkPage from './../modules/work/components/index';
import AboutPage from './../modules/about/components/index';
import BlogPage from './../modules/blog/components/index';
import Post from './../modules/post/components/index';

const Sroutes = [
  {
    path: '/',
    exact: true,
    component: WorkPage,
    key: 'workroute'
  },
  {
    path: '/about',
    component: AboutPage,
    key: 'aboutrute'
  },
  {
    path: '/blogs',
    exact: true,
    component: BlogPage,
    key: 'blogroute'
  },
  {
    path: '/post/:id',
    component: Post,
    key: 'postglogroute'
  }
];

export default Sroutes;
