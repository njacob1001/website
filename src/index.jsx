//import "@babel/polyfill";
import React from 'react';
import ReactGA from 'react-ga';
import { hydrate } from 'react-dom';
import App from './modules/app';
import './style.scss';

//Importar archivos adiconales
import '!file-loader?name=[name].[ext]&outputPath=./assets/img/!./assets/img/og-image.jpg'; //eslint-disable-line
import '!file-loader?name=[name].[ext]&outputPath=./!./assets/robots.txt'; //eslint-disable-line
ReactGA.initialize('UA-127796428-2');
hydrate(<App />, document.getElementById('root'));

if (typeof window === 'undefined') {
  global.window = {}
}

//ENABLE THIS SCRIPT ON PRODUCTION MODE
